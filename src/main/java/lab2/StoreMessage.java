package lab2;
import org.apache.axis.client.Call;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.*;
import java.util.*;

public class StoreMessage {

    static String fileXML;
    static SOAPMessage reqMess, respMess;


    public static void main(String[] args) throws Exception {
        StoreMessage client = new StoreMessage();
        client.start();
        client.createSOAPRequest();
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();
        URL endpoint = new URL("http://localhost:8080/axis/services/StoreService");
        respMess = connection.call(reqMess, endpoint);

        displayMessage(respMess);
    }

    public void createSOAPRequest() throws SOAPException, TransformerException{

        MessageFactory messageFactory = MessageFactory.newInstance();
        reqMess = messageFactory.createMessage();


        SOAPPart soapPart = reqMess.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        SOAPHeader soapHeader = soapEnvelope.getHeader();
        SOAPBody soapBody = soapEnvelope.getBody();
        soapHeader.detachNode();

        Name bodyName = soapEnvelope.createName("GetFile");
        SOAPBodyElement bodyElement = soapBody.addBodyElement(bodyName);
        Name name = soapEnvelope.createName("file");
        SOAPElement fileName = bodyElement.addChildElement(name);
        fileName.addTextNode(fileXML);

    }

    public static void displayMessage(SOAPMessage mess) throws SOAPException{
        SOAPBody body = mess.getSOAPBody();
        Iterator<Node> it = body.getChildElements();
        SOAPBodyElement bodyElement = (SOAPBodyElement) it.next();
        System.out.println(bodyElement);

    }

    public void start(){

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("enter the file XML:");
            fileXML = bufferedReader.readLine();
        }catch (IOException ex){
            ex.printStackTrace();
        }

    }
}
