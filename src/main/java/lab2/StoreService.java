package lab2;

import org.apache.axis.MessageContext;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.axis.message.SOAPHeader;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class StoreService {

    public String getFileName(SOAPEnvelope req) throws Exception{

        SOAPBodyElement reqBody = (SOAPBodyElement) req.getBodyElements().get(0);
        String str = "";

        NodeList nodes = reqBody.getChildNodes();
        for(int i = 0; i < nodes.getLength(); i++){
            Node currentNode = nodes.item(i);
            if (currentNode instanceof Element){
                Element element = (Element) currentNode;
                Text textNode = (Text) element.getFirstChild();
                str = textNode.getData().trim();
            }
        }
        return str;
    }

    public SOAPEnvelope createSOAPResponse(String fileName, SOAPEnvelope resp) throws Exception{
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("c:/"+fileName);

        MessageContext messageContext = MessageContext.getCurrentContext();
        SOAPMessage responseMessage = messageContext.getMessage();

        SOAPPart soapPart = responseMessage.getSOAPPart();
        SOAPEnvelope envelope = (SOAPEnvelope) soapPart.getEnvelope();
        SOAPHeader header = (SOAPHeader) resp.getHeader();

        SOAPBody body = resp.getBody();
        header.detachNode();

        SOAPBodyElement docElement = (SOAPBodyElement) body.addDocument(document);

        return envelope;
    }

    public void storeSevice(SOAPEnvelope req, SOAPEnvelope resp) throws Exception{
        String fileName = getFileName(req);
        resp = createSOAPResponse(fileName, resp);
    }
}
