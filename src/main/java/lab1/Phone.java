package lab1;

import java.io.*;
import java.util.*;

public class Phone {

    public String phone_name(String phone){
        String name = "";
        String line = "";

        try {
            FileReader fr = new FileReader("C:/phones.txt");
            BufferedReader in = new BufferedReader(fr);
            while ((line = in.readLine())!=null){
                String[] str = line.split(" ");
                if (str[0].equals(phone)){
                    name = str[1];
                    break;
                }else{
                    name = "no such telephone";
                }
            }
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return name;
    }

    public String name_phone(String name){
        String phone = "";
        String line = "";

        try{
            FileReader fr = new FileReader("");
            BufferedReader in = new BufferedReader(fr);

            while ((line=in.readLine())!=null){
                String[] str = line.split(" ");
                if (str[1].equals(name)){
                    phone = str[0];
                    break;
                }else { phone = "no such name";}
            }
        }catch(IOException ex){
                ex.printStackTrace();
        }
        return phone;
    }

}
